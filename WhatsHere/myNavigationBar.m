//
//  myNavigationBar.m
//  ASBA
//
//  Created by Sabin.MS on 31/07/15.
//  Copyright (c) 2015 Jimmy. All rights reserved.
//

#import "myNavigationBar.h"

@implementation myNavigationBar


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    UIImage *image=[[UIImage imageNamed:@"nav_bar.png"]
                    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    [image drawInRect:CGRectMake(0,0,self.frame.size.width,self.frame.size.height+10)];
}


@end
