//
//  WebViewController.m
//  ASBA
//
//  Created by Sabin.MS on 28/07/15.
//  Copyright (c) 2015 Jimmy. All rights reserved.
//

#import "WebViewController.h"
#import "MBProgressHUD.h"

@interface WebViewController ()
{
    MBProgressHUD *HUD;
}

@end

@implementation WebViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;

 
//    UINavigationBar *navbar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
//    self.navigationItem.backBarButtonItem.title=@"back";
//    self.navigationController.navigationBar.tintColor=[UIColor blueColor];
//    self.navigationController.navigationItem.title=@"dsfzgv";
//    [self.view addSubview:navbar];
    [self setTitle:_stringTitle];

    NSURL *url=[NSURL URLWithString:_stringURL];
    NSURLRequest *requestObj=[NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    _webView.delegate=self;
    HUD=[[MBProgressHUD alloc] initWithView:self.view];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [HUD show:YES];
    [self.view addSubview:HUD];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [HUD hide:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
